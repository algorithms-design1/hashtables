/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.mavenproject1;

/**
 *
 * @author PC Sakda
 */
public class Node {
    private int key;
    private String value;
    private int N=100;
    private String[] arr = new String[N];
    public Node() {
        
    }
    public int hash(int key){
        return key % N;
    }
    
    public void put(int key, String value){
        arr[hash(key)] = value;
    }
    
    public String get(int key){
        return arr[hash(key)];
    }
    
    public void remove(int key){
        arr[hash(key)] = null;
    }
}
