/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.mavenproject1;

/**
 *
 * @author informatics
 */
public class Hash {

    public static void main(String[] args) {
        Node node = new Node();
        // กรณียังไม่กำหนดค่า 
        System.out.println(node.get(0)); //ผลลัพธ์จะออกมาเป็น null

        // กรณีเพิ่มค่า key = 1 , value = Sakda 
        node.put(1, "Sakda");
        System.out.println(node.get(1)); //ปริ้น key ที่ 1 ผลลัพธ์จะออกมาเป็น Sakda
        System.out.println(node.get(2));// ปริ้น key ที่ 2 ผลลัพธ์จะออกเป็น null เพราะ key ที่ 2 ยังไม่มีค่า

        // กรณีลบ key ที่ 1
        node.remove(1);
        System.out.println(node.get(1)); // ผลลัพธ์จะออกเป็น null เพราะลบไปแล้ว
    }
}
